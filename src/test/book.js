import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import chaiNock from 'chai-nock';
import chaiAsPromised from 'chai-as-promised';
import path from 'path';
import nock from 'nock';

import server from '../server';
import resetDatabase from '../utils/resetDatabase';
chai.use(chaiHttp);
chai.use(chaiNock);
chai.use(chaiAsPromised);
chai.should();

describe('première série de test intégration', () => {
    beforeEach(() => {
        const pathBooks = path.join(__dirname, '../data/books.json');
        const initialStructure = {
            books: []
          };
        resetDatabase(pathBooks, initialStructure)
    }); 

    it(' should return a book ', done => {
        chai
          .request(server)
          .get('/book')
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.books.should.be.a('array');
            res.body.books.length.should.be.equal(0);
            done();
          });
        });

        it('should post a book ', done => {
            chai
              .request(server)
              .post('/book')
              .end((err, res) => {
                res.should.have.status(200); 
                res.body.should.have.property('message').eql('book successfully added');
                done();
              });
            });            
});

describe('Seconde serie de test intégration', () => {
    const id = '0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9'
    const title= 'Coco raconte Channel 2'
    const years=1990
    const pages=400
    beforeEach(() => {
        const pathBooks = path.join(__dirname, '../data/books.json');
        const initialStructure =  {
            books:
            [
                {
                    id: id,
                    title: title,
                    years: years,
                    pages: pages
                }
            ]
        }
        resetDatabase(pathBooks, initialStructure) 
    });

    it('it should UPDATE a book given the id', done => {
              chai.request(server)
              .put('/book/' + id)
              .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('book successfully updated');
                    done();
              });
    });

    it('it should DELETE a book given the id', done => {
        chai.request(server)
        .delete('/book/' + id)
        .end((err, res) => {
              res.should.have.status(200);
              res.body.should.have.property('message').eql('book successfully deleted');
              done();
        });
    });

    it('it should return a book given the id', done => {
        chai.request(server)
        .get('/book/' + id)
        .end((err, res) => {
              res.should.have.status(200);
              res.body.should.have.property('message').eql('book fetched');
              res.body.should.be.a('object');
              //console.log(res.body);
              res.body.book.title.should.be.a('string')
              res.body.book.should.have.property('id').eql(id);
              res.body.book.should.have.property('title').eql(title);
              res.body.book.years.should.be.a('number');
              res.body.book.should.have.property('years').eql(years);
              res.body.book.pages.should.be.a('number');
              res.body.book.should.have.property('pages').eql(pages);
              
            
              done();
        });
    });
});

// Série de tests unitaires*******************************************
describe('GET---première série Test unitaire', () => {
it('should be status 200 ', (done) => {
  nock(' http://localhost:8080')
  .get('/book')
  .reply(200, {
    "status" : 200,
  })
  chai.request('http://localhost:8080')
  .get('/book')
  .end(function(err, res){
    //assert that the mocked response is returned
    //expect(res.body.status).to.equal(200); 
    res.should.have.status(200);
    done();
  })
})

it('should be an array ', (done) => {
  nock(' http://localhost:8080')
  .get('/book')
  .reply(200, {
    "books" : [],
  })
  chai.request('http://localhost:8080')
  .get('/book')
  .end(function(err, res){
    //assert that the mocked response is returned
    //expect(res.body.books).to.be.an('Array'); 
    res.body.books.should.be.a('array');
    done();
  })
})
});

describe('Post--- Test unitaire', () => {
  it('should post a status 200 ', (done) => {
    nock(' http://localhost:8080')
    .post('/book')
    .reply(200, {
      "status" : 200,
    })
    chai.request('http://localhost:8080')
    .post('/book')
    .end(function(err, res){
      res.should.have.status(200);
      done();
    })
  })
  
  it('should post a message ', (done) => {
    nock(' http://localhost:8080')
    .post('/book')
    .reply(200, {
      "message" :'book successfully added' ,
    })
    chai.request('http://localhost:8080')
    .post('/book')
    .end(function(err, res){ 
      //assert that the mocked response is returned
      res.body.should.have.property('message').eql('book successfully added');
      //expect(res.body.message).to.equal('book successfully added');
      done();
    })
  })
  });

  describe('Put--- Test unitaire', () => {
    it('should update----status 200 ', (done) => {
      nock(' http://localhost:8080')
      .put('/book')
      .reply(200, {
        "status" : 200,
      })
      chai.request('http://localhost:8080')
      .put('/book')
      .end(function(err, res){
        res.should.have.status(200);
        done();
      })
    })
    
    it('should update book------a message ', (done) => {
      nock(' http://localhost:8080')
      .put('/book')
      .reply(200, {
        "message" :'book successfully updated' ,
      })
      chai.request('http://localhost:8080')
      .put('/book')
      .end(function(err, res){ 
        //assert that the mocked response is returned
        res.body.should.have.property('message').eql('book successfully updated');
        //expect(res.body.message).to.equal('book successfully updated');
        done();
      })
    })
    });

    describe('Delete--- Test unitaire', () => {
      it('should delete with status 200 ', (done) => {
        nock(' http://localhost:8080')
        .delete('/book')
        .reply(200, {
          "status" : 200,
        })
        chai.request('http://localhost:8080')
        .delete('/book' )
        .end(function(err, res){
          res.should.have.status(200);
          done();
        })
      })
      
      it('should delete ', (done) => {
        nock(' http://localhost:8080')
        .delete('/book')
        .reply(200, {
          "message" :'book successfully deleted' ,
        })
        chai.request('http://localhost:8080')
        .delete('/book')
        .end(function(err, res){ 
          res.body.should.have.property('message').eql('book successfully deleted');
          //expect(res.body.message).to.equal('book successfully deleted');
          done();
        })
      })
      });

      describe('GET---deuxième série Test unitaire', () => {
        it('should be error status 400 ', (done) => {
          nock(' http://localhost:8080')
          .get('/book')
          .reply(400, {
            "status" : 400,
            "message" :'error fetching books' ,
          })
          chai.request('http://localhost:8080')
          .get('/book')
          .end(function(err, res){
            //assert that the mocked response is returned
            //expect(res.body.status).to.equal(200); 
            res.should.have.status(400);
            res.body.should.have.property('message').eql('error fetching books');
            done();
          })
        })
      });

      describe('Post---série 2 Test unitaire', () => {
        it('should post error a status 400 ', (done) => {
          nock(' http://localhost:8080')
          .post('/book')
          .reply(400, {
            "status" : 400,
          })
          chai.request('http://localhost:8080')
          .post('/book')
          .end(function(err, res){
            res.should.have.status(400);
            done();
          })
        })
        
        it('should post a message ', (done) => {
          nock(' http://localhost:8080')
          .post('/book')
          .reply(400, {
            "message" :'error adding the book' ,
          })
          chai.request('http://localhost:8080')
          .post('/book')
          .end(function(err, res){ 
            //assert that the mocked response is returned
            res.body.should.have.property('message').eql('error adding the book');
            //expect(res.body.message).to.equal('error adding the book');
            done();
          })
        })
        });

        describe('Put---série 2 Test unitaire', () => {
          it('should update error----status 400 ', (done) => {
            nock(' http://localhost:8080')
            .put('/book')
            .reply(400, {
              "status" : 400,
            })
            chai.request('http://localhost:8080')
            .put('/book')
            .end(function(err, res){
              res.should.have.status(400);
              done();
            })
          })
          
          it('should update book------a message ', (done) => {
            nock(' http://localhost:8080')
            .put('/book')
            .reply(400, {
              "message" :'error updating the book' ,
            })
            chai.request('http://localhost:8080')
            .put('/book')
            .end(function(err, res){ 
              //assert that the mocked response is returned
              res.body.should.have.property('message').eql('error updating the book');
              //expect(res.body.message).to.equal('error updating the book');
              done();
            })
          })
          });

          describe('Delete error---série 2 Test unitaire', () => {
            it('should delete with status 400 ', (done) => {
              nock(' http://localhost:8080')
              .delete('/book')
              .reply(400, {
                "status" : 400,
              })
              chai.request('http://localhost:8080')
              .delete('/book' )
              .end(function(err, res){
                res.should.have.status(400);
                done();
              })
            })
            
            it('should delete error ', (done) => {
              nock(' http://localhost:8080')
              .delete('/book')
              .reply(400, {
                "message" :'error deleting the book' ,
              })
              chai.request('http://localhost:8080')
              .delete('/book')
              .end(function(err, res){ 
                res.body.should.have.property('message').eql('error deleting the book');
                //expect(res.body.message).to.equal('error deleting the book');
                done();
              })
            })
            });

      


    
 

// tout les packages et fonction nescessaire au test sont importé ici, bon courage

// fait les Tests d'integration en premier
